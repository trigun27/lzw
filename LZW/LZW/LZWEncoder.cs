﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LZW
{
    public class LZWEncoder
    {
        public Dictionary<string, int> dict = new Dictionary<string, int>();
        ANSI table = null;
             
        int codeLen = 8;
        public LZWEncoder()
        {
            table = new ANSI();
            dict = table.Table;
        }
        
         
        public string EncodeToCodes(string input)
        {
            StringBuilder sb = new StringBuilder();

            int i = 0;
            string w = "";
            while (i < input.Length)
            {
                w = input[i].ToString();

                i++;                

                while (dict.ContainsKey(w) && i < input.Length)
                {
                    w += input[i];
                    i++;
                }

                if (dict.ContainsKey(w) == false)                
                {
                    string matchKey = w.Substring(0, w.Length - 1);
                    sb.Append(dict[matchKey] +  ", ");

                    dict.Add(w, dict.Count);
                    i--;
                }
                else 
                {
                    sb.Append(dict[w] + ", ");
                }
            }

            return sb.ToString(); 
        }

        public string Encode(byte[] input)
        {
            StringBuilder sb = new StringBuilder();

            int i = 0;
            char ch = ' ';
            string w = "";
         
            while (i < input.Length)
            {
                 //ch = Convert.ToChar(input[i]);
                 w = Encoding.Default.GetString(new byte[1] { input[i] }); 
                 i++;

                 if (i == 561)
                 {
                     i--;
                     i++;
                 }

                 while (dict.ContainsKey(w) && i < input.Length)
                 {
                     //ch = Convert.ToChar(input[i]);
                     w += Encoding.Default.GetString(new byte[1] { input[i] }); ;
                     i++;
                 }

                 if (dict.ContainsKey(w) == false)                
                 {
                     string matchKey = w.Substring(0, w.Length - 1);
                     sb.Append(Convert.ToString(dict[matchKey], 2).FillWithZero(codeLen));

                     if (Convert.ToString(dict.Count, 2).Length > codeLen)
                         codeLen++;

                     dict.Add(w, dict.Count);
                     i--;
                 }
                 else
                 {                    
                     sb.Append(Convert.ToString(dict[w], 2).FillWithZero(codeLen));

                     if (Convert.ToString(dict.Count, 2).Length > codeLen)
                         codeLen++;   
                 
                 }
             }

            return sb.ToString();
        }
        
        public byte[] EncodeToByteList(byte[] input)
        {
            var encodedInput = Encode(input);
            return encodedInput.ToByteArray();
        }

    }
}
