﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LZW
{
    internal class Program
    {
        private static void Main(string[] args)
        {
 
            Console.WriteLine("Generate ANSI table ...");
            
            ANSI ascii = new ANSI();
            ascii.WriteToFile();


            Console.WriteLine("ANSI table generated.");

            Console.WriteLine("Start encoding " + args[0] + " ...");

           // string text = File.ReadAllText(fileToCompress, System.Text.ASCIIEncoding.Default);
            byte[] bytetext = File.ReadAllBytes(args[0]);
            LZWEncoder encoder = new LZWEncoder();  
           
            byte[] b = encoder.EncodeToByteList(bytetext);

            var encodedFile = "Output" + args[0] + ".lzw.txt";
            File.WriteAllBytes(encodedFile, b);


            var decodedFile = "Decoded_" + args[0];
            Console.WriteLine("File " + args[0] + " encoded to " + encodedFile);

            Console.WriteLine("Start decoding " + encodedFile);

            
            LZWDecoder decoder = new LZWDecoder();
            byte[] bo = File.ReadAllBytes(encodedFile);
            string decodedOutput = decoder.DecodeFromCodes(bo);
            File.WriteAllText(decodedFile, decodedOutput, System.Text.Encoding.Default);

            Console.WriteLine("File " + encodedFile + " decoded to " + decodedFile);
    

        }
    }
}
